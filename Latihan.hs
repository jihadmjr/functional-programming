import Data.List

-- 1. This list comp will return [5,6,7]
listcompre = [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

-- 2. Find primes 
primes = sieve [2..100]
    where sieve (p:ps) = p : sieve [ x | x <- ps, x `mod` p /= 0 ]

-- 3. QuickSort
quickSort []      = []
quickSort [x]     = [x]
quickSort (x:xs)  = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]

-- 4. Find Pytha
pythaTriple = [(x,y,z) | z <- [5..], y <- [1..z-1], x <- [1..y-1], x*x + y*y == z*z ]

-- 5. Find permutation
perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls\\[x]) ] 

-- 6. Divisor
divisor n = [x| x <- [1 .. n] , n `mod` x ==0 ]
-- >>> perms [1,2]
-- [[1,2],[2,1]]
--

-- 7. Monad, will return: Just "Unicorn"
animalMap :: [(String, String)]
animalMap = [("Pony","Lion"),("Lion","Manticore"),("Manticore","Unicorn"),("Unicorn","Lepricon")]

sugaryFriendLookup :: [(String, String)] -> Maybe String
sugaryFriendLookup animalMap = do
    ponyFriend    <- lookup "Pony" animalMap
    ponyFriend'   <- lookup ponyFriend animalMap
    ponyFriend''  <- lookup ponyFriend' animalMap
    return ponyFriend''


-- maxList [] = 0
maxList xs = foldr (max) xs
 where max x y | x >= y = x | otherwise = y