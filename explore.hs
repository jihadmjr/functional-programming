
--Exploring Haskell

-- Membuat list dengan batas tak hingga
evenlist = [2,4..]

-- Fungsi untuk menghasilkan suatu list dengan mengalikan 3 setiap x dari list 1,2,.. dengan batas x*5 ≤ 20
listTimes3 = [x * 3 | x <- [1,2..], x*5 <= 20]

-- Method takeWhile
evenUpTo100 = takeWhile (<=100) [2,4..]

-- 
multOfList' xy = foldl (*) 1 xy
--
mult x ys = map (x*) ys


-- Tuples
-- Zip, jika terdapat list yang lebih panjang dibandingkan list lainnya, maka ukuran tuple = panjang list terpendek
names = ["a", "b", "c"]
nums = [1,2,3]
zipList = zip names nums

-- Factorial, recursive call
factorials :: Int -> Int
factorials 0 = 1 -- base case
factorials n = n * factorials (n-1)


-- IF in haskell
-- Function for check an input is odd
isOdd n
    | n `mod` 2 == 0 = False
    | otherwise = True

-- 
yourGrade :: Int -> String
yourGrade age
    | (age < 10) = "Anak-anak"
    | (age > 10) && (age <= 14) = "SMP"
    | (age > 14) && (age <= 18) = "SMA"
    | otherwise = "Kuliah"

-- get First item in string
-- ex: getFirst "Jihad" will return "J"
getFirst :: String -> String
getFirst [] = "Empty"
getFirst all@(x:xs) = [x]

multBy4 :: [Int] -> [Int]
multBy4 [] = []
multBy4 xs = map (*4) xs

-- double item in list 1.. 10 
dbl1To10 = map (\x -> x*2) [1..10]