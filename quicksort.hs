quickSort []      = []
quickSort [x]     = [x]
quickSort (x:xs)  = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]
