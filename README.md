# Functional Programming

Functional Programming course using Haskell

## Thanks to
1. Haskell Tutorial - https://www.youtube.com/watch?v=02_H3LjqMr8
2. Haskell Exercise - http://www.cse.chalmers.se/edu/year/2018/course/TDA555_Introduktion_Till_Funktionell_Programmering/ex-week5.html
3. Learn You a Haskell for Great Good http://learnyouahaskell.com/
4. The Haskell School of Expression http://www.cs.yale.edu/homes/hudak/SOE/index.htm