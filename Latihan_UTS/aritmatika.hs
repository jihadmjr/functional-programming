data Expr = Num Integer
          | Add Expr Expr
          | Mul Expr Expr
          
eval :: Expr -> Integer
eval (Num n) = n
eval (Add a b) = eval a + eval b
eval (Mul a b) = eval a * eval b

showExpr :: Expr -> String
showExpr (Num n) = show n
showExpr (Add a b) = showExpr a ++ "+" ++ showExpr b
showExpr (Mul a b) = showExpr a ++ "*" ++ showExpr b

-- >>> showExpr (Mul (Num 1) (Add (Num 2) (Num 3)))
-- "1*2+3"
--
