-- Suatu tipe data untuk mendeskripsikan expression
data Exp = C Float
          | V String
          | Exp :+ Exp
          | Exp :- Exp
          | Exp :* Exp
          | Exp :/ Exp
          | Let String Exp Exp
            deriving Show

subst :: String -> Exp -> Exp -> Exp
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

eval :: Exp -> Float
eval (C x) = x
eval (e1 :+ e2)    = eval e1 + eval e2
eval (e1 :- e2)    = eval e1 - eval e2
eval (e1 :* e2)    = eval e1 * eval e2
eval (e1 :/ e2)    = eval e1 / eval e2
eval (Let v e0 e1) = eval (subst v e0 e1)
eval (V _)         = 0.0

-- Implementasi eval untuk Fold

eval (add e1 e2)  = (eval e1) + (eval e2) 
eval (mult e1 e2) = (eval e1) * (eval e2)
eval (subt e1 e2) = (eval e1) - (eval e2)
eval (div e1 e2)  = (eval e1) / (eval e2)


-- Expression Fold 
foldExp (add, mult, subt, div) (C x) = x
foldExp (add, mult, subt, div) (x:+y) = add (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:*y) = mult (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:-y) = subt (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
foldExp (add, mult, subt, div) f (x:/y) = div (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)

newEval = foldExp (add, mult, subt, div)
          where add = (+)
                mult = (*)
                subt = (-)
                div = (/)


-- foldExp (+) (\x->x) 

-- RUNNER

-- >>> eval (Let "x" (C 3.0) (Let "y" (V "x") (Let "x" (C 2.0) (V "y"))))
-- 2.0
--


-- >>> eval (Let "x" (C 5.0) ((V "x") :+ (V "x")))
-- 10.0
--

-- >>> eval (((C 5) :+ (C 7) :* (C 4)))
-- 48.0
--


-- >>> exp1 = Let "x" (C 7) ((C 5) :+ (C 7) :* (V "x"))
-- >>> exp1
-- >>> eval exp1
-- Let "x" (C 7.0) ((C 5.0 :+ C 7.0) :* V "x")
-- 84.0
--

-- >>> eval ((C 10.0 :+ (C 8.0 :/ C 2.0)) :* (C 7.0 :- C 4.0))
-- 42.0
--

-- >>> eval (((C 5.0) :+ (C 7) :* (C 4)))
-- 48.0
--

-- >>> eval (V "a")
-- 0.0
--

-- >>> (C 1.0 :+ 1.0)
-- <interactive>:353:12: error:
--     • No instance for (Fractional Exp) arising from the literal ‘1.0’
--     • In the second argument of ‘(:+)’, namely ‘1.0’
--       In the expression: (C 1.0 :+ 1.0)
--       In an equation for ‘it’: it = (C 1.0 :+ 1.0)
--
