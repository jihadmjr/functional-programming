-- Max Tiga
maxTiga :: Int -> Int -> Int -> Int 
maxTiga x y z = max x (max y z)

-- MaxList
maxList :: [Int] -> Int
maxList (x:xs) = foldl max x xs

-- SumList
sumList :: [Int] -> Int
sumList (x:xs) = foldl (+) x xs

-- Misteri
misteri xs ys =  (map (\x -> map (\y -> (x,y)) ys) xs)
-- >>> misteri [1,2,3] [4,5,6]
-- [[(1,4),(1,5),(1,6)],[(2,4),(2,5),(2,6)],[(3,4),(3,5),(3,6)]]
--
{-- Fungsi map (\x) akan melakukan iterasi setiam elemen pada list x yang kemudian di petakan (map) menuju 
    list y yang juga di petakan menjadi suatu pair. Jika tidak di lakukan concat maka output sementara akan
    menjadi [(1,4),(1,5),(1,6)], [(2,4),(2,5),(2,6)], [(3,4),(3,5),(3,6)]
    Concat akan menggabungkan 2 list menjadi list sehingga output menjadi 
    [(1,4),(1,5),(1,6),(2,4),(2,5),(2,6),(3,4),(3,5),(3,6)]
--}

-- Quicksort
quickSort :: [Int] -> [Int]
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]

-- MergeSort
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) | x <= y    = x:merge xs (y:ys)
                    | otherwise = y:merge (x:xs) ys

msort :: Ord a => [a] -> [a]
msort [] = []
msort [a] = [a]
msort xs = merge (msort (firstHalf xs)) (msort (secondHalf xs))

firstHalf  xs = let { n = length xs } in take (div n 2) xs
secondHalf xs = let { n = length xs } in drop (div n 2) xs

-- Pythagoras
pytha = [(x,y,z) | z <- [5 .. 100], y <- [4 .. z-1], x <- [3 .. y-1], x*x + y*y == z*z]

-- Primes
primes =    sieve [2..]
            where sieve (x:xs) = x : sieve [y | y<-xs, y `mod` x /= 0]
-- >>> primes
-- >>> :t flip
-- flip :: (a -> b -> c) -> b -> a -> c
--

simple x y z = (x + y)*z
myflip f x y = f y x
newSimple = myflip simple

-- >>> sim = myflip x z
-- >>> sim 1 2 3

fibonacci = fibo (1, 1)
    where fibo (x, y) = x : fibo (y, x + y)
    

factorial = fact [1..]
    where fact (x:y:xs) = x : fact (y*x:xs)






