--  (*) Define the length function using map and sum.
array_length :: [a] -> Int
array_length xs = sum (map (\x -> 1) xs)

-- Sum Squares
sumSquares :: Integer -> Integer
sumSquares n = foldl (+) 0 $ map (\x -> x*x) [1..n]

-- List Comprehensions
-- 1. [ x+1 | x <- xs ]
lc_1  xs = map (+1) xs

-- 2. [ x+y | x <- xs, y <-ys ]
lc_2  xs ys = (map (\x -> map (\y -> x+y) ys) xs)

-- 3. [ x+2 | x <- xs, x > 3 ]
lc_3 xs = map (+2) (filter (>3) xs)

-- 4. [ x+3 | (x,_) <- xys ]’
lc_4 xys = map (\(x,_) -> x+3) xys  
-- or
lc_4' xys = map ((+3) . fst)  xys

-- 5. [ x+4 | (x,y) <- xys, x+y < 5 ]

-- 6. [ x+5 | Just x <- mxs ]

data Maybe a = Nothing | Just a