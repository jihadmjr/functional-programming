import Data.List
import System.IO

-- How to compile or run it: 
{--
1. Type: ghc --make IOScreen
2. Type: ./IOScreen
--}
main = do
    putStrLn "What's your name"
    name <- getLine
    putStrLn ("Hello " ++ name)